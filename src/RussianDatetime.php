<?php
namespace ru\kozalo;
use DateTime;

/**
 * Class RussianDatetime
 *
 * Used to output a human-readable representing of date or time.
 *
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 * @package ru\kozalo
 * @example tests/SmartDatetimeTests.php There is a test class where you can see how to use it.
 * @license MIT
 * @uses DateTime
 * @see RussianDatetimeTests
 */
class RussianDatetime {
    /**
     * timestamp
     *
     * Contains a UNIX timestamp.
     *
     * @var int
     */
    private $timestamp;


    /**
     * declensions
     *
     * It consists of the following fields: 'minutes', 'hours', 'days', 'weeks', 'months', and 'years'.
     * All fields are also arrays which consists of 3 words representing some declinations.
     * This array is used in the GetCorrectDeclension() function. Obviously, huh?
     *
     * @var array
     */
    private static $declensions = [
        'minutes' => [
            'минуту',
            'минуты',
            'минут'
        ],
        'hours' => [
            'час',
            'часа',
            'часов'
        ],
        'days' => [
            'день',
            'дня',
            'дней'
        ],
        'weeks' => [
            'неделю',
            'недели',
            'недель'
        ],
        'months' => [
            'месяц',
            'месяца',
            'месяцев'
        ],
        'years' => [
            'год',
            'года',
            'лет'
        ]
    ];


    
    /**
     * Constructor.
     *
     * @param int $timestamp
     */
    public function __construct($timestamp=null)
    {
        $this->timestamp = (isset($timestamp))
            ? (int)$timestamp
            : time();
    }


    /**
     * Now
     *
     * Creates a SmartDatetime object with current time.
     *
     * @return RussianDatetime
     */
    public static function Now()
    {
        return new self();
    }


    /**
     * CreateFromTimestamp
     *
     * Creates a SmartDatetime object from timestamp.
     *
     * @param int $timestamp
     * @return RussianDatetime
     */
    public static function CreateFromTimestamp($timestamp)
    {
        return new self($timestamp);
    }


    /**
     * CreateFromString
     *
     * Creates a SmartDatetime object from string.
     *
     * Uses the {@link http://php.net/manual/en/function.strtotime.php strtotime()} function, so expects to be given a string containing an English date format.
     *
     * @param string $dateStr
     * @return RussianDatetime
     * @uses strtotime
     */
    public static function CreateFromString($dateStr)
    {
        return new self(strtotime($dateStr));
    }


    /**
     * GetTimestamp
     *
     * Returns the timestamp containing inside the object.
     *
     * @param void
     * @return int
     */
    public function GetTimestamp()
    {
        return $this->timestamp;
    }


    /**
     * GetDateForMysql
     *
     * Returns the string, which you can use in your sql queries.
     *
     * @param void
     * @return string
     */
    public function GetDateForMysql()
    {
        return date('Y-m-j H:i:s', $this->GetTimestamp());
    }


    /**
     * GetDiffInRussian
     *
     * Returns a well-formatted human-readable Russian string which represents the difference between now and containing timestamp.
     *
     * @param void
     * @return string
     */
    public function GetDiffInRussian()
    {
        $now = time();
        $timestamp = $this->GetTimestamp();

        $present = new DateTime();
        $present->setTimestamp($now);
        $then = new DateTime();
        $then->setTimestamp($timestamp);

        $diff = $present->diff($then);

        if ($diff->y > 0)
            $correctCollocation = self::GetCorrectDeclension($diff->y, "years");
        elseif ($diff->m > 0)
            $correctCollocation = self::GetCorrectDeclension($diff->m, "months");
        elseif ($diff->d >= 7) {
            $weekDiff = ceil(abs($now - $timestamp) / 604800);
            $correctCollocation = self::GetCorrectDeclension($weekDiff, "weeks");
        }
        elseif ($diff->d > 0)
            $correctCollocation = self::GetCorrectDeclension($diff->d, "days");
        elseif ($diff->h > 0)
            $correctCollocation = self::GetCorrectDeclension($diff->h, "hours");
        elseif ($diff->i > 0)
            $correctCollocation = self::GetCorrectDeclension($diff->i, "minutes");
        else
            $correctCollocation = "Меньше минуты";
        
        return "$correctCollocation назад";
    }


    /**
     * GetCorrectDeclension
     *
     * Returns a correct declension in Russian. Something like 'Минуту', or '2 часа', or '25 дней'.
     *
     * @param int $number
     * @param string $declensionMode One of the following: "minutes", "hours", "days", "weeks", "months", or "years".
     * @return string
     */
    public static function GetCorrectDeclension($number, $declensionMode)
    {
        if (!key_exists($declensionMode, self::$declensions))
            return false;

        $mod = $number % 10;
        $declensions = self::$declensions[$declensionMode];

        if ((int)$number === 1)
            return mb_strtoupper(mb_substr($declensions[0], 0, 1)) . mb_substr($declensions[0], 1);
        elseif (($mod >= 2 && $mod <= 4 && !($number >= 12 && $number <= 14)) || ($number >= 2 && $number <= 4))
            return "$number {$declensions[1]}";
        elseif ((int)$mod === 1 && $number != 11)
            return "$number {$declensions[0]}";
        else
            return "$number {$declensions[2]}";
    }
}