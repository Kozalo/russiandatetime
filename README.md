RussianDatetime
===============

Для русскоговорящих
-------------------

Что делает этот класс и почему он русский? Ну наверное потому, что основное его предназначение —
это выводить надписи вида "меньше минуты назад", "12 минут назад" или "3 недели назад". Часто же встречали такие?  
Вы просто задаёте ему время, и он выдаёт Вам нужную строку, сравнивая её с текущим временем! Ну не красота ли?!  

В остальном же он, по сути, является лишь небольшой надстройкой над стандартным типом DateTime.
Ну на данный момент по крайней мере.  
Разве что ещё у него есть метод для выдачи даты в формате, который сразу можно скормить в MySQL.  

Итак, создать его можно аж 4-мя способами:  
1. С помощью конструктора, который либо принимает *TIMESTAMP*, либо создаёт объект с текущим временем.  
2. Статическим методом `Now()`.  
3. Статический метод `CreateFromTimestamp([timestamp])` полностью аналогичен конструктору.  
4. Статический метод `CreateFromString(<строка>)` использует функцию `strtotime()` и передаёт полученный *TIMESTAMP* в конструктор. Так что отсылаю к документации на эту функцию или примеру в тестах.  

Ну а дальше что? Ну есть 3 метода:  
1. `GetTimestamp()` просто возвращает *TIMESTAMP*.  
2. `GetDateForMysql()` отдаёт дату в следующем формате: `Y-m-j H:i:s`.  
3. `GetDiffInRussian()` — основной метод, собственно, ради которого всё и затевалось. Возвращает разницу во времени (по сравнению с текущим) в человекочитаемом формате.  

Также ещё есть статический метод `GetCorrectDeclension(<число>, <режим_работы>)`. Он больше предназначен для внутренних нужд, но вдруг пригодится?  
Он берёт число и склоняет под него второе слово. Режимы работы могут быть следующими: `"minutes"`, `"hours"`, `"days"`, `"weeks"`, `"months"`, `"years"`. На все остальные вернёт *false*. В итоге получается строка вроде `5 часов`.  

Вот и всё, что умеет этот класс ¯\\_(ツ)_/¯  

*Автор: Леонид Козарин (<kozalo@yandex.ru>)*  
*© Kozalo.Ru, 2017*


In English
----------

You need this class if you have a necessity to print something like "12 minutes ago" in Russian.  

So, what should you do to get a string like "12 минут назад"?  
1. Create an instance. You can use the constructor which takes a timestamp (or omit the parameter if you want to get a current timestamp). Another way to get an instance is the static methods: `Now()`, `CreateFromTimestamp([timestamp])`, and `CreateFromString(<string>)`. The second one works in exactly the same way as the constructor. The last uses `strtotime()` function and also passes the result into the constructor. See the tests for an example.  
2. Just call the method `GetDiffInRussian()` to get the string! It's very easy, isn't it?  

Also, you can be interested in the method `GetDateForMysql()`. It returns the date and time in the following format: `Y-m-j H:i:s`.  

There is one more method, but I doubt you'll need to use it ever. Mostly, it's supposed for internal usage, and it just returns the right declensions.  

*Author: Leonid Kozarin (<kozalo@yandex.ru>)*  
*© Kozalo.Ru, 2017*