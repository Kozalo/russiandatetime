<?php
namespace ru\kozalo\tests;

require("php-classes/ru/kozalo/SmartDatetime.php");

use ru\kozalo\RussianDatetime;

/**
 * Class RussianDatetimeTests
 *
 * Test class for SmartDatetime
 *
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 * @package ru\kozalo\tests
 * @see RussianDatetime
 */
class RussianDatetimeTests extends \PHPUnit_Framework_TestCase {

    /**
     * testGetDiffInRussian
     *
     * Testing GetDiffInRussian() method.
     *
     * @param void
     * @return void
     */
    public function testGetDiffInRussian()
    {
        $given = [
            RussianDatetime::CreateFromString("-30 seconds"),
            RussianDatetime::CreateFromString("-11 minutes"),
            RussianDatetime::CreateFromString("-12 minutes"),
            RussianDatetime::CreateFromString("-30 minutes"),
            RussianDatetime::CreateFromString("-2 hours"),
            RussianDatetime::CreateFromString("-8 hours"),
            RussianDatetime::CreateFromString("-11 hours"),
            RussianDatetime::CreateFromString("-12 hours"),
            RussianDatetime::CreateFromString("-1 day"),
            RussianDatetime::CreateFromString("-2 days"),
            RussianDatetime::CreateFromString("-5 days"),
            RussianDatetime::CreateFromString("-11 days"),
            RussianDatetime::CreateFromString("-1 week"),
            RussianDatetime::CreateFromString("-3 weeks"),
            RussianDatetime::CreateFromString("-1 month"),
            RussianDatetime::CreateFromString("-6 months"),
            RussianDatetime::CreateFromString("-11 months"),
            RussianDatetime::CreateFromString("-12 months"),
            RussianDatetime::CreateFromString("-1 year"),
            RussianDatetime::CreateFromString("-2 years"),
            RussianDatetime::CreateFromString("-11 years"),
            RussianDatetime::CreateFromString("-12 years"),
            RussianDatetime::CreateFromString("-21 years"),
            RussianDatetime::CreateFromString("-22 years"),
        ];

        $expectedResults = [
            'Меньше минуты назад',
            '11 минут назад',
            '12 минут назад',
            '30 минут назад',
            '2 часа назад',
            '8 часов назад',
            '11 часов назад',
            '12 часов назад',
            'День назад',
            '2 дня назад',
            '5 дней назад',
            '2 недели назад',
            'Неделю назад',
            '3 недели назад',
            "Месяц назад",
            '6 месяцев назад',
            '11 месяцев назад',
            'Год назад',
            'Год назад',
            '2 года назад',
            '11 лет назад',
            '12 лет назад',
            '21 год назад',
            '22 года назад',
        ];

        for ($i = 0; $i < count($given); $i++)
            $this->assertEquals($given[$i]->GetDiffInRussian(), $expectedResults[$i]);
    }

}